//布局js
function resize() {
    let width = window.screen.width;// 获取当前屏幕的宽度
    const basicValue = 750; //设计稿上的分辨率大小
    
    //放大100倍，为了方便计算rem大小 + => 拼接符 50px 375 / 750 = 0.5 * 100  150px / 100 
    document.documentElement.style.fontSize = (width / basicValue) * 100 + 'px';
}

//加上以下代码是为了切换设备时能时刻刷新动态fontSize，从而得到正确的布局
window.addEventListener('resize', function () {
    resize();
});
window.addEventListener('DOMContentLoaded', function () {
   resize();
});